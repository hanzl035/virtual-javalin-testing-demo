import org.junit.jupiter.api.Test;

import java.io.*;
import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class DaoTest {

    private static Scanner scanner;
    private static StringBuffer buffer;



    @Test
    public void temp() throws FileNotFoundException, SQLException {
        Dao.url = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";

        try (
            Connection connection = DriverManager.getConnection(
                Dao.url
            )
        ) {
            PreparedStatement statement = SQLFetcher.fetch(
                    "sql/create.psql", connection
            );
            statement.executeUpdate();

            PreparedStatement insertStatement = connection.prepareStatement(
                "INSERT INTO users (fullname, hobby, age)\n" +
                        "\tVALUES (\n" +
                        "\t\t'David Hanzlik',\n" +
                        "\t\t'Video Games',\n" +
                        "\t\t29\n" +
                        "\t);"
            );
            insertStatement.executeUpdate();

            List<Map<String, Object>> collector = Dao.getUsers();
            System.out.println(collector);

        }

        Dao.getUsers();
    }
}
