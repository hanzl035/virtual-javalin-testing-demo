import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class SQLFetcher {

    private static Scanner scanner;
    private static StringBuffer buffer;

    public static PreparedStatement fetch(String resourceUrl, Connection connection) throws SQLException {
        buffer = new StringBuffer();
        scanner = new Scanner(
            SQLFetcher.class.getResourceAsStream("sql/create.psql")
        );
        while (scanner.hasNextLine()) {
            buffer.append(scanner.nextLine());
            buffer.append("\n");
        }
        return connection.prepareStatement(
                buffer.toString()
        );
    }
}
