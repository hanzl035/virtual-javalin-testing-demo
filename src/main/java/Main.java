import io.javalin.Javalin;

public class Main {

    public static void main(String[] args) {
        initialize().start();
    }

    public static Javalin initialize() {
        Javalin app = Javalin.create();
        app.get("/users", Controller::getUsers);
        return app;
    }
}
