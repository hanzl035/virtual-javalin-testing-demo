import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Dao {
    public static String url = System.getenv("dbConnection");
    private static String username = System.getenv("dbUsername");
    private static String password = System.getenv("dbPassword");

    public static List<Map<String, Object>> getUsers() {
        try (
            Connection connection = DriverManager.getConnection(
                url,
                username,
                password
            )
        ) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM users;"
            );
            List<Map<String, Object>> collector = new ArrayList<>();
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                collector.add(
                        Map.of(
                                "fullname", results.getString("fullname"),
                                "hobby", results.getString("hobby"),
                                "age", results.getInt("age")
                        )
                );
            }
            return collector;
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


}
